package com.itkpi.java.contest.cities.solution.solver;

import com.itkpi.java.contest.cities.solution.utils.Pair;

import java.util.*;

import static com.itkpi.java.contest.cities.solution.SolutionUtils.getFirstChar;
import static com.itkpi.java.contest.cities.solution.SolutionUtils.getLastChar;
import static com.itkpi.java.contest.cities.solution.StatisticsUtils.*;

public class LookAheadSolver implements CitySolver {
    private final Statistics stats;

    public LookAheadSolver(Statistics stats) {
        this.stats = stats;
    }

    @Override
    public List<String> solveCitiesGame(List<String> allCitiesList) {
        ArrayList<String> names = new ArrayList<>(allCitiesList);
        ArrayList<String> result = new ArrayList<>();
        String name = getAnyName(names);
        process(name, names, result);
        char nextChar = getLastChar(name);
        while (true) {
            Optional<String> optName = getNextName(names, nextChar);
            if (!optName.isPresent()) {
                break;
            }
            name = optName.get();
            nextChar = getLastChar(name);
            process(name, names, result);
        }
        return result;
    }

    private void process(String name, List<String> all, List<String> result) {
        all.remove(name);
        result.add(name);
        final Pair<Character, Character> pair = new Pair<>(getFirstChar(name), getLastChar(name));
        decrement(stats.getFirst(), pair.getFirst());
        decrement(stats.getLast(), pair.getSecond());
        decrement(stats.getPairs(), pair);
        cache.clear();
    }

    private <T> void decrement(Map<T, Long> map, T key) {
        map.put(key, map.get(key) - 1);
    }

    private Optional<String> getNextName(List<String> names, char firstChar) {
        Optional<String> max = names.stream()
                .filter(name -> getFirstChar(name) == firstChar)
                .max(Comparator.comparingLong(str -> weight1(names, str)));
        if (!max.isPresent())
            return max;
        String best = max.get();
        return names.stream()
                .filter(name -> getFirstChar(name) == getFirstChar(best) && getLastChar(name) == getLastChar(best))
                .max(Comparator.comparingInt(String::length));
    }

    private String getAnyName(List<String> names) {
        return names.stream()
                .max(Comparator.comparingLong(str -> weight1(names, str)))
                .orElseThrow(IllegalStateException::new);
    }

    private long weight(String name) {
        return stats.getFirst().getOrDefault(getLastChar(name), 0L);
    }

    private final HashMap<Pair<Integer, Character>, Long> cache = new HashMap<>();

    private long weight1(List<String> names, String name) {
        final char lastChar = getLastChar(name);
        Pair<Integer, Character> key = new Pair<>(names.size(), lastChar);
        if (cache.containsKey(key)) {
            return cache.get(key);
        }
        long result = stats.getFirst().getOrDefault(lastChar, 0L)
                + names.stream()
                .filter(str -> getFirstChar(str) == lastChar)
                .mapToLong(this::weight)
                .sum();
        cache.put(key, result);
        return result;
    }
}
