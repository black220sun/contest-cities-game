package com.itkpi.java.contest.cities.solution;

import com.itkpi.java.contest.cities.solution.utils.Pair;

import java.io.PrintStream;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.itkpi.java.contest.cities.solution.SolutionUtils.getFirstChar;
import static com.itkpi.java.contest.cities.solution.SolutionUtils.getLastChar;

public class StatisticsUtils {
    public static Statistics getStats(List<String> names) {
        Map<Character, Long> first = names.stream()
                .map(SolutionUtils::getFirstChar)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        Map<Character, Long> last = names.stream()
                .map(SolutionUtils::getLastChar)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        Map<Pair<Character, Character>, Long> pairs = names.stream()
                .map(name -> new Pair<>(getFirstChar(name), getLastChar(name)))
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        return new Statistics(first, last, pairs);
    }

    public static class Statistics {
        private final Map<Character, Long> first;
        private final Map<Character, Long> last;
        private final Map<Pair<Character, Character>, Long> pairs;

        private Statistics(Map<Character, Long> first, Map<Character, Long> last, Map<Pair<Character, Character>, Long> pairs) {
            this.first = first;
            this.last = last;
            this.pairs = pairs;
        }

        public Map<Character, Long> getFirst() {
            return first;
        }

        public Map<Character, Long> getLast() {
            return last;
        }

        public Map<Pair<Character, Character>, Long> getPairs() {
            return pairs;
        }

        public void dump() {
            dump(System.out);
        }

        private void dump(PrintStream stream) {
            stream.println("FIRST");
            first.forEach((ch, amount) -> stream.format("%c : %d\n", ch, amount));
            stream.println("LAST");
            last.forEach((ch, amount) -> stream.format("%c : %d\n", ch, amount));
            stream.println("PAIRS");
            pairs.entrySet().stream()
                    .sorted(Comparator.comparingInt(e -> e.getKey().getFirst()))
                    .forEach(entry -> stream.format("(%c, %c) : %d\n",
                            entry.getKey().getFirst(), entry.getKey().getSecond(), entry.getValue()));
        }
    }
}
