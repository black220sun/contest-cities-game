package com.itkpi.java.contest.cities.solution.solver;

import java.util.*;

import static com.itkpi.java.contest.cities.solution.SolutionUtils.getFirstChar;
import static com.itkpi.java.contest.cities.solution.SolutionUtils.getLastChar;

public class PathfindingSolver implements CitySolver {
    @Override
    public List<String> solveCitiesGame(List<String> allCitiesList) {
        return computePath(allCitiesList, getStartIndex(allCitiesList));
    }

    private int getStartIndex(List<String> allCitiesList) {
        return getFirstIndex(allCitiesList.stream()
                .max(Comparator.comparingInt(String::length))
                .orElse("Azua"));
    }

    private int getFirstIndex(String name) {
        return getFirstChar(name) - 'a';
    }

    private int getLastIndex(String name) {
        return getLastChar(name) - 'a';
    }

    private List<String> computePath(List<String> names, int startIndex) {
        final int MAX_CHARS = 26;
        PriorityQueue<String>[][] matrix = new PriorityQueue[MAX_CHARS][MAX_CHARS];
        for (String name : names) {
            int first = getFirstIndex(name);
            int last = getLastIndex(name);
            if (matrix[first][last] == null) {
                matrix[first][last] = new PriorityQueue<>(Comparator.comparingInt(str -> -str.length()));
            }
            matrix[first][last].add(name);
        }

        List<String> result = new ArrayList<>();
        int first = startIndex;
        while (true) {
            int maxIndex = 0;
            int maxLength = 0;
            for (int last = 0; last < MAX_CHARS; ++last) {
                PriorityQueue<String> queue = matrix[first][last];
                if (queue != null && !queue.isEmpty()) {
                    int length = queue.peek().length();
                    if (length > maxLength) {
                        maxLength = length;
                        maxIndex = last;
                    }
                }
            }
            final PriorityQueue<String> queue = matrix[first][maxIndex];
            if (queue == null || queue.peek() == null) {
                break;
            }
            String name = queue.poll();
            result.add(name);
            first = getLastIndex(name);
        }

        result = addSelfLoops(result, matrix);

        return result;
    }

    private List<String> addSelfLoops(List<String> result, PriorityQueue<String>[][] matrix) {
        return result.stream()
                .map(Collections::singletonList)
                .reduce(new ArrayList<>(), (acc, list) -> {
                    acc.addAll(list);
                    String name = list.get(0);
                    int last = getLastIndex(name);
                    PriorityQueue<String> queue = matrix[last][last];
                    if (queue != null) {
                        acc.addAll(queue);
                        queue.clear();
                    }
                    return acc;
                });
    }
}
