package com.itkpi.java.contest.cities.solution.solver;

import java.util.List;

public interface CitySolver {
    List<String> solveCitiesGame(List<String> allCitiesList);
}
