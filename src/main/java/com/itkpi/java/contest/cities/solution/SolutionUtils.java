package com.itkpi.java.contest.cities.solution;

import com.itkpi.java.contest.cities.solution.utils.Pair;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class SolutionUtils {
    private static final HashMap<String, Character> FIRST_CHAR_CACHE = new HashMap<>();
    private static final HashMap<String, Character> LAST_CHAR_CACHE = new HashMap<>();

    // compute chars once to remove redundant `Map::contains` calls inside `get{First,Last}Char`
    public synchronized static void initMaps(List<String> names) {
        for (String name : names) {
            FIRST_CHAR_CACHE.put(name, computeFirstChar(name));
            LAST_CHAR_CACHE.put(name, computeLastChar(name));
        }
    }

    public synchronized static char computeFirstChar(String name) {
        return name.toLowerCase().charAt(0);
    }

    public synchronized static char computeLastChar(String name) {
        return name.charAt(name.length() - 1);
    }

    public static char getFirstChar(String str) {
        return FIRST_CHAR_CACHE.get(str);
    }

    public static char getLastChar(String str) {
        return LAST_CHAR_CACHE.get(str);
    }

    /**
     * Try to merge solutions.
     * Replace a single name by a longer sequence with the same start/end character.
     */
    public static Pair<List<String>, List<String>> mergeSolutions(List<String> base, Collection<List<String>> solutions) {
        List<String> unused = new ArrayList<>();
        for (List<String> sequence : solutions) {
            char first = getFirstChar(sequence.get(0));
            char last = getLastChar(sequence.get(sequence.size() - 1));
            if (first == last) {
                inline(base, sequence, first);
            } else if (last == getFirstChar(base.get(0))) {
                base.addAll(0, sequence);
            } else if (first == getLastChar(base.get(base.size() - 1))) {
                base.addAll(sequence);
            } else {
                int length = sequence.stream().mapToInt(String::length).sum();
                // find the shortest name that could be replaced with current sequence
                final Pair<Integer, Integer> indexes = getShortestSubChain(base, first, last, length);
                final int index = indexes.getFirst();
                if (index != -1) {
                    for (int i = index; i <= indexes.getSecond(); ++i) {
                        unused.add(base.remove(index));
                    }
                    base.addAll(index, sequence);
                } else {
                    unused.addAll(sequence);
                }
            }
        }
        return new Pair<>(base, unused);
    }

    private static void inline(List<String> solution, List<String> subchain, char ch) {
        for (int i = 0; i < solution.size(); ++i) {
            if (getFirstChar(solution.get(i)) == ch) {
                solution.addAll(i, subchain);
                break;
            }
        }
    }

    private static Pair<Integer, Integer> getShortestSubChain(List<String> solution, char first, char last, int maxScore) {
        int score;
        int bestFrom = -1, bestTo = -1, bestScore = maxScore;
        String name;
        for (int i = 0; i < solution.size(); ++i) {
            name = solution.get(i);
            if (getFirstChar(name) == first) {
                score = 0;
                for (int j = i; j < solution.size(); ++j) {
                    name = solution.get(j);
                    score += name.length();
                    if (getLastChar(name) == last) {
                        if (score < bestScore) {
                            bestFrom = i;
                            bestTo = j;
                            bestScore = score;
                        }
                        break;
                    }
                }
            }
        }
        return new Pair<>(bestFrom, bestTo);
    }

    public static List<String> listDiff(List<String> all, List<String> toRemove) {
        return all.stream()
                .filter(name -> !toRemove.contains(name))
                .collect(Collectors.toList());
    }
}
