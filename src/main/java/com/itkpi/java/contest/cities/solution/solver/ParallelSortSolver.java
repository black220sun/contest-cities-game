package com.itkpi.java.contest.cities.solution.solver;

import com.itkpi.java.contest.cities.solution.SolutionValidator;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

public class ParallelSortSolver implements CitySolver {
    private final int limit;
    private final int score;
    private final int iterations;

    public ParallelSortSolver(int limit, int score, int iterations) {
        this.limit = limit;
        this.score = score;
        this.iterations = iterations;
    }

    @Override
    public List<String> solveCitiesGame(List<String> allCitiesList) {
        return Stream.generate(() -> allCitiesList)
                .limit(limit)
                .parallel()
                .map(name -> new SortSolver(score, iterations).solveCitiesGame(name))
                .max(Comparator.comparingInt(SolutionValidator::evaluateSolution))
                .orElse(Collections.emptyList());
    }
}
