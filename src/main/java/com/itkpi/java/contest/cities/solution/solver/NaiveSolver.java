package com.itkpi.java.contest.cities.solution.solver;

import com.itkpi.java.contest.cities.solution.utils.CharPair;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.itkpi.java.contest.cities.solution.SolutionUtils.getFirstChar;
import static com.itkpi.java.contest.cities.solution.SolutionUtils.getLastChar;

public class NaiveSolver {
    public List<String> solveCitiesGame(List<String> allCitiesList)
    {
        // we are interested in first/last letters only
        final Map<CharPair, List<String>> unified = allCitiesList.stream()
                .collect(Collectors.groupingBy(name -> new CharPair(getFirstChar(name), getLastChar(name))));
        // build separate chains of kind { "A...a" -> "A...a" -> ... }
        final Map<Character, List<String>> loops = buildSelfLoops(unified);
        // connect chains into a single sequence
        final List<List<String>> values = new ArrayList<>(loops.values());
        // some shit begins here
        final List<String> result = new ArrayList<>(values.remove(0));
        char currentLast = getLastChar(result.get(0));
        boolean updated;
        do {
            updated = false;
            for (List<String> names : values) {
                char newFirst = getFirstChar(names.get(0));
                final List<String> possibleConnections = unified.get(new CharPair(currentLast, newFirst));
                if (possibleConnections == null)
                    continue;
                final String connection = possibleConnections.stream()
                        .max(Comparator.comparingInt(String::length))
                        .orElseThrow(IllegalStateException::new);
                updated = true;
                result.add(connection);
                result.addAll(names);
                values.remove(names);
                currentLast = newFirst;
                break;
            }
        } while (updated);
        return result;
    }

    private Map<Character, List<String>> buildSelfLoops(Map<CharPair, List<String>> map) {
        return map.keySet().stream()
                .filter(CharPair::isLoop)
                .collect(Collectors.toMap(CharPair::getFirst, map::get));
    }
}
