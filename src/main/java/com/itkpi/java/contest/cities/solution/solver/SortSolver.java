package com.itkpi.java.contest.cities.solution.solver;

import com.itkpi.java.contest.cities.solution.SolutionUtils;
import com.itkpi.java.contest.cities.solution.SolutionValidator;
import com.itkpi.java.contest.cities.solution.utils.Pair;

import java.util.*;
import java.util.function.ToIntFunction;

import static com.itkpi.java.contest.cities.solution.SolutionUtils.getFirstChar;
import static com.itkpi.java.contest.cities.solution.SolutionUtils.getLastChar;

public class SortSolver implements CitySolver {
    private final Random random;
    private final HashMap<String, Integer> randomWeights = new HashMap<>();
    private final int solutionStopScore;
    private final int stopIteration;

    public SortSolver(int solutionStopScore, int iteration) {
        this.solutionStopScore = solutionStopScore;
        this.stopIteration = iteration;
        this.random = new Random();
    }

    public SortSolver(int iteration, long seed) {
        this.solutionStopScore = Integer.MAX_VALUE;
        this.stopIteration = iteration;
        this.random = new Random(seed);
    }

    @Override
    public List<String> solveCitiesGame(List<String> allCitiesList)
    {
        // init phase, prepare array
        String[] names = initNamesArray(allCitiesList, str -> -str.length());
        // build possible solutions
        List<List<String>> solutions = getSolutions(names);
        // try to merge solutions
        Pair<List<String>, List<String>> merged = mergeSolutions(solutions);
        // get current longest solution

        int iteration = 0;
        List<String> bestSolution = merged.getFirst();
        int bestScore = SolutionValidator.evaluateSolution(bestSolution);
        int score;
        do {
            ++iteration;
            merged = randomIteration(merged);
            score = SolutionValidator.evaluateSolution(merged.getFirst());
            if (score > bestScore) {
                bestScore = score;
                bestSolution = merged.getFirst();
            }
        } while (score < solutionStopScore && iteration < stopIteration);
        return bestSolution;
    }

    /**
     * Randomly shuffle unused names, compute chains, try to inline chains into current solution
     * @param merged pair { solution, unused names }
     * @return pair { solution, unused names }
     */
    public Pair<List<String>, List<String>> randomIteration(Pair<List<String>, List<String>> merged) {
        String[] names = initNamesArray(merged.getSecond(), this::randomValue);
        return SolutionUtils.mergeSolutions(merged.getFirst(),  getSolutions(names));
    }

    private String[] initNamesArray(List<String> allCitiesList, ToIntFunction<String> mapper) {
        String[] names = allCitiesList.toArray(new String[0]);
        randomWeights.clear();
        Arrays.sort(names, Comparator.comparingInt(mapper));
        sort(names);
        return names;
    }

    private int randomValue(String str) {
        if (!randomWeights.containsKey(str)) {
            randomWeights.put(str, random.nextInt());
        }
        return randomWeights.get(str);
    }

    private Pair<List<String>, List<String>> mergeSolutions(List<List<String>> solutions) {
        List<String> base = solutions.stream()
                .max(Comparator.comparingInt(list -> list.stream().mapToInt(String::length).sum()))
                .orElseThrow(IllegalStateException::new);
        solutions.remove(base);
        return SolutionUtils.mergeSolutions(base, solutions);
    }

    private List<List<String>> getSolutions(String[] names) {
        int index = 0;
        ArrayList<List<String>> solutions = new ArrayList<>();
        while (index < names.length) {
            List<String> solution = getSolution(names, index);
            index += solution.size();
            solutions.add(solution);
        }
        return solutions;
    }

    private List<String> getSolution(String[] names, int startIndex) {
        ArrayList<String> solution = new ArrayList<>();
        solution.add(names[startIndex]);
        char lastChar = getLastChar(names[startIndex]);
        for (int i = startIndex + 1; i < names.length; ++i) {
            String name = names[i];
            if (lastChar == getFirstChar(name)) {
                lastChar = getLastChar(name);
                solution.add(name);
            } else {
                break;
            }
        }
        return solution;
    }

    // TODO(speed-up)
    private void sort(String[] names) {
        String prevCity, nextCity;
        int index, lastSwapped = 1;
        boolean usefulSwap;
        char lastChar;
        for (int i = 0; i < names.length - 1; ++i) {
            usefulSwap = false;
            for (int j = lastSwapped + 1; j < names.length; ++j) {
                prevCity = names[j - 1];
                nextCity = names[j];
                lastChar = getLastChar(prevCity);
                if (getFirstChar(nextCity) != lastChar) {
                    index = findNextCityIndex(names, j, lastChar);
                    if (index != -1) {
                        swap(names, j, index);
                        lastSwapped = j;
                        usefulSwap = true;
                    } else {
                        break;
                    }
                }
            }
            if (!usefulSwap) {
                break;
            }
        }
    }

    private void swap(String[] names, int pos1, int pos2) {
        String tmp  = names[pos1];
        names[pos1] = names[pos2];
        names[pos2] = tmp;
    }

    private int findNextCityIndex(String[] names, int index, char ch) {
        for (int i = index + 1; i < names.length; ++i) {
            if (getFirstChar(names[i]) == ch) {
                return i;
            }
        }
        return -1;
    }
}
