package com.itkpi.java.contest.cities.solution.solver;

import com.itkpi.java.contest.cities.solution.SolutionValidator;
import com.itkpi.java.contest.cities.solution.utils.Pair;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.itkpi.java.contest.cities.solution.SolutionUtils.mergeSolutions;

public class IterativePathSolver implements CitySolver {
    private PathfindingSolver solver = new PathfindingSolver();

    @Override
    public List<String> solveCitiesGame(List<String> allCitiesList) {
        final ArrayList<String> unused = new ArrayList<>(allCitiesList);
        List<String> solution = solver.solveCitiesGame(unused);
        unused.removeAll(solution);
        int score, lastScore = 0;
        while (true) {
            final List<String> result = solver.solveCitiesGame(unused);
            if (result.isEmpty()) {
                unused.clear();
                unused.addAll(allCitiesList);
                unused.removeAll(solution);
                score = SolutionValidator.evaluateSolution(solution);
                if (score == lastScore) {
                    break;
                } else {
                    lastScore = score;
                    continue;
                }
            }
            final Pair<List<String>, List<String>> pair = mergeSolutions(solution, Collections.singletonList(result));
            final List<String> second = pair.getSecond();
            unused.removeAll(result);
            if (!result.equals(second)) {
                // merged successfully, add removed sub-chain to the list of unused names
                unused.addAll(second);
            }
            solution = pair.getFirst();
        }
        return solution;
    }
}
