package com.itkpi.java.contest.cities.solution.solver;

import com.itkpi.java.contest.cities.solution.SolutionValidator;
import com.itkpi.java.contest.cities.solution.utils.Pair;

import java.util.*;

import static com.itkpi.java.contest.cities.solution.SolutionUtils.*;

/**
 * Approach: build solution, cut some sub chains, build new solution from unused names, merge two solutions.
 * New solution should be built with higher score than removed one.
 */
public class RetrySortSolver implements CitySolver {
    private final SortSolver solver;
    private final int removedChains;
    private final int skippedMatches;
    private int failsAllowed;

    /**
     * @param chains amount of sub chains to be removed from current solution
     * @param skipped N-th shortest match will be removed:
     *                for sequence "ab-ba-ac-ca-ad-da":
     *                skipped = 1: ab-ba is removed
     *                skipped = 2: ab-ba-ac-ca is removed
     * @param failsAllowed max amount of iterations without updating best score
     */
    public RetrySortSolver(int chains, int skipped, int failsAllowed) {
        // TODO inject initial solver somewhere?
        solver = new SortSolver(14500, 1000);
        removedChains = chains;
        skippedMatches = skipped;
        this.failsAllowed = failsAllowed;
    }

    @Override
    public List<String> solveCitiesGame(List<String> allCitiesList) {
        List<String> solution = solver.solveCitiesGame(allCitiesList);
        int score = SolutionValidator.evaluateSolution(solution);
        Pair<List<String>, List<String>> result;

        int failedTry = 0;
        do {
            final Pair<List<String>, List<String>> pair = removeSubChains(solution);
            final int removedScore = SolutionValidator.evaluateSolution(pair.getSecond());
            final int removed = pair.getSecond().size();
            if (removed > 800) {
                // too long to compute
                continue;
            }
            final List<String> unused = new ArrayList<>(allCitiesList);
            unused.removeAll(pair.getFirst());
            final List<String> newChain = new SortSolver((int) Math.round(removedScore * 1.15), removed + 100).solveCitiesGame(unused);
            result = mergeSolutions(pair.getFirst(), Collections.singletonList(newChain));
            int newScore = SolutionValidator.evaluateSolution(result.getFirst());

            if (newScore > score) {
                score = newScore;
                solution = result.getFirst();
            } else {
                ++failedTry;
            }
        } while (failedTry < failsAllowed);
        return solution;
    }

    /**
     *
     * @param solution
     * @return pair { solution without some sub-chains, removed names }
     */
    private Pair<List<String>, List<String>> removeSubChains(List<String> solution) {
        final ArrayList<String> result = new ArrayList<>(solution);
        final ArrayList<String> unused = new ArrayList<>();
        final Random random = new Random();
        for (int i = 0; i < removedChains; ++i) {
            final int startIndex = random.nextInt(result.size() / 9 * 7);
            int lastMatchIndex = 0;
            int matches = 0;
            final char last = getLastChar(result.get(startIndex));
            for (int j = startIndex + 2; j < result.size(); ++j) {
                if (getFirstChar(result.get(j)) == last) {
                    lastMatchIndex = j;
                    if (++matches >= skippedMatches) {
                        break;
                    }
                }
            }
            if (lastMatchIndex != 0) {
                for (int index = startIndex + 1; index < lastMatchIndex; ++index) {
                    unused.add(result.get(index));
                }
                result.removeAll(unused);
            }
        }
        return new Pair<>(result, unused);
    }
}
