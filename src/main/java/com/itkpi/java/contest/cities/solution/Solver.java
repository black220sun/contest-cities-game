package com.itkpi.java.contest.cities.solution;

import com.itkpi.java.contest.cities.solution.solver.*;
import com.itkpi.java.contest.cities.solution.utils.Pair;

import java.util.*;

import static com.itkpi.java.contest.cities.solution.SolutionUtils.*;
import static com.itkpi.java.contest.cities.solution.StatisticsUtils.getStats;

public class Solver
{

    /**
     * This method should order cities names (strings) from allCitiesList argument
     * into longest chain possible following the rules of the "Cities game".
     *
     * <br><br>
     *
     * <b> Cities game rules: </b>
     * <li> Cities names should be ordered to the chain in which every next city name starting with the last letter of the previous city name. </li>
     * <li> The first city name can be any. </li>
     * <li> Each city name should be real. </li>
     * <li> All city names in the chain should be unique. </li>
     *
     * <br>
     *
     * <b>Example:</b> London, Naga, Aurora, Aswan
     *
     * <br><br>
     *
     * <b>Additional requirements:</b>
     * Time limit is 2 minutes
     */
    public List<String> solveCitiesGame(List<String> allCitiesList)
    {
        // STATS:
        // max ever possible - 34141
        // loops - 1903
        // **********************************
        // NaiveSolver() - 2075 // does not work with loops optimization, saved as history
        // PathfindingSolver() - 9870
        // IterativePathSolver() - 14723
        // SortSolver(1200, 1980798640254865148L) - 16715
        // RetrySortSolver(11, 2, 3) - 16500+
        // RetrySortSolver(3, 11, 2) - 16600+
        // ParallelRetrySolver(6) - 16600+
        // ParallelSortSolver(6, 14800, 1000) - 16680+
        // LookAheadSolver(getStats(pair.getSecond())) - 15618

        // cache first/last char for each name
        SolutionUtils.initMaps(allCitiesList);

        // try to speed-up computations by removing loops that don't affect the order at all
        // TODO 1) works for SortSolver only? 2) only 223 names is removed, does it speed-up something at all?
        final Pair<Map<Character, List<String>>, List<String>> pair = removeSelfLoops(allCitiesList);
        // real solver logic
        final List<String> solution = new SortSolver(1200, 1980798640254865148L)
                .solveCitiesGame(pair.getSecond());
        // inline removed loops
        return mergeLoops(solution, pair.getFirst());
    }

    /**
     *
     * @param names list of names to be filtered
     * @return pair of { map char -> loop chain, list of unused names }
     */
    private Pair<Map<Character, List<String>>, List<String>> removeSelfLoops(List<String> names) {
        final HashMap<Character, List<String>> loops = new HashMap<>();
        final List<String> unused = new ArrayList<>();
        for (String name : names) {
            final char ch = getFirstChar(name);
            if (ch == getLastChar(name)) {
                if (!loops.containsKey(ch)) {
                    loops.put(ch, new ArrayList<>());
                }
                loops.get(ch).add(name);
            } else {
                unused.add(name);
            }
        }
        return new Pair<>(loops, unused);
    }

    private List<String> mergeLoops(List<String> solution, Map<Character, List<String>> loops) {
        final ArrayList<String> result = new ArrayList<>(solution);
        for (char ch : loops.keySet()) {
            result.stream()
                    .filter(name -> getFirstChar(name) == ch)
                    .findFirst()
                    .ifPresent(name -> result.addAll(result.indexOf(name), loops.get(ch)));
        }
        return result;
    }
}
