package com.itkpi.java.contest.cities.solution.solver;

import com.itkpi.java.contest.cities.solution.SolutionValidator;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

public class ParallelRetrySolver implements CitySolver {
    private final int limit;

    public ParallelRetrySolver(int limit) {
        this.limit = limit;
    }

    @Override
    public List<String> solveCitiesGame(List<String> allCitiesList) {
        return Stream.generate(() -> allCitiesList)
                .limit(limit)
                .parallel()
                .map(name -> new RetrySortSolver(3, 5, 3).solveCitiesGame(name))
                .max(Comparator.comparingInt(SolutionValidator::evaluateSolution))
                .orElse(Collections.emptyList());
    }
}
