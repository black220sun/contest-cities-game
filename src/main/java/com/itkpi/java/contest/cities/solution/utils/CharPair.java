package com.itkpi.java.contest.cities.solution.utils;


public class CharPair extends Pair<Character, Character> {

    public CharPair(char first, char last) {
       super(first, last);
    }

    public char getLast() {
        return getSecond();
    }

    public boolean isLoop() {
        return getFirst().equals(getLast());
    }

    @Override
    public String toString() {
        return "CharPair{" +
                "first=" + getFirst() +
                ", last=" + getLast() +
                '}';
    }
}
